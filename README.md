# openboard-flatpak

Build openboard for flatpak

1. Checkout openboard code repo  

git clone https://github.com/OpenBoard-Org/OpenBoard.git  
git clone https://github.com/OpenBoard-Org/OpenBoard-Importer.git  
git clone https://github.com/OpenBoard-Org/OpenBoard-ThirdParty.git  

2. Execute below command  
flatpak-builder build-dir org.flatpak.openboard.json


Follow below link to know about how to build flatpak  
https://docs.flatpak.org/en/latest/first-build.html  
https://docs.flatpak.org/en/latest/building-introduction.html

Follow below link for some QT application specific instruction  
https://community.kde.org/Guidelines_and_HOWTOs/Flatpak
