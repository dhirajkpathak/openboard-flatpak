#!/bin/sh


cd OpenBoard-ThirdParty/freetype
qmake freetype.pro -spec linux-g++ # optionally, use linux-g++-64 for Ubuntu 20.04
make

cd ../quazip
qmake quazip.pro -spec linux-g++ # optionally, use linux-g++-64 for Ubuntu 20.04
make
cd ../xpdf/xpdf-3.04
./configure --with-freetype2-library="../../freetype/lib/linux" --with-freetype2-includes="../../freetype/freetype-2.6.1/include"
cd ..
qmake xpdf.pro -spec linux-g++ # optionally, use linux-g++-64 for Ubuntu 20.04
make


cd ../../OpenBoard-Importer
qmake OpenBoardImporter.pro -spec linux-g++ # optionally, use linux-g++-64 for Ubuntu 20.04
make

cd ../OpenBoard
qmake OpenBoard.pro -spec linux-g++-64 # using Ubuntu 20.04, can appear a warning: sh: 1: Syntax error: "&" unexpected, three times each
make